# Tutoriels

Vous pouvez suivre les tutoriels `Matlab` et `Julia` en ligne en cliquant sur le bouton (ou lien) suivant : 

[<p align="center"><img src="go.jpg" alt="Lien vers les tutoriels" width="200" align="center"/></p>](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.irit.fr%2Ftoc%2Fens-n7%2Ftutoriels%2Fenvironnements/master?urlpath=lab/tree/src)

## Matlab 

Ouvrir le fichier `matlab.ipynb` dans le répertoire `matlab` et suivre les instructions.

## Julia

Le répertoire `julia` contient :

* `notebook_Julia.pdf` : un pdf d'introduction à `Julia`,
* `intro-julia.ipynb` : un notebook d'introduction à `Julia`,
* `ode-exemple-sol.ipynb` : une introduction à l'intégration numérique avec `Julia`.




